<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\debug\debug
{
	require_once('globals.hh');
	
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\
	{
		config\Config,
		debug\PHPConsole\PHPConsole
	};
	
	use \Kint;
	use \Exception;
	
	/**
	 * Debug Plugin.
	 * 
	 * This is usef
	 */
	<<singleton>>
	class Debug extends Plugin
	{
		private PHPConsole $console;
		
		private Map<string,bool> $config=Map
		{
			'consoleEnabled'	=>true,
			'screenDumpEnabled'	=>true
		};
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Debug
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		public function __construct(Config $config)
		{
			parent::__construct();
			$this->config->set('consoleEnabled',$config->get('debug.consoleEnabled') ?? true);
			$this->config->set('screenDumpEnabled',$config->get('debug.screenDumpEnabled') ?? true);
			
			if ($this->config->get('consoleEnabled'))
			{
				$this->console=PHPConsole::getInstance();
			}
		}
		
		public function handleException(Exception $exception):void
		{
			if ($this->config->get('consoleEnabled'))
			{
				$this->getConsole()->handleException($exception);
			}
			if ($this->config->get('screenDumpEnabled'))
			{
				Kint::dump($exception);
			}
		}
		
		public function getConsole():PHPConsole
		{
			return $this->console;
		}
		
		public function dump(mixed $var):this
		{
			if ($this->config->get('consoleEnabled'))
			{
				$this->getConsole()->debug($var);
			}
			if ($this->config->get('screenDumpEnabled'))
			{
				Kint::dump($var);
			}
			return $this;
		}
		
		public function enableConsole():this
		{
			$this->config->set('consoleEnabled',true);
			return $this;
		}
		
		public function disableConsole():this
		{
			$this->config->set('consoleEnabled',false);
			return $this;
		}
		
		public function enableScreenDump():this
		{
			$this->config->set('screenDumpEnabled',true);
			return $this;
		}
		
		public function disableScreenDump():this
		{
			$this->config->set('screenDumpEnabled',false);
			return $this;
		}
	}
}
