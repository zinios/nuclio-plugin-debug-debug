<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace
{
	use nuclio\plugin\debug\debug\Debug;
	
	function debug():Debug
	{
		return Debug::getInstance();
	}
	
	function dump(mixed $var):Debug
	{
		$debug=Debug::getInstance();
		$debug->dump($var);
		return $debug;
	}
}
